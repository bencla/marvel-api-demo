package sk.danibenc.marvelapidemo.adapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import sk.danibenc.marvelapidemo.fragment.DashboardFragment;
import sk.danibenc.marvelapidemo.fragment.FavouritesFragment;

/**
 * View pager adapter for main activity.
 *
 * @author bencurova
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    /**
     * ID of the {@link DashboardFragment}
     */
    public static final int DASHBOARD_FRAGMENT_ID = 0;

    /**
     * ID of the {@link FavouritesFragment}
     */
    public static final int FAVOURITES_FRAGMENT_ID = 1;

    private static final int FRAGMENT_COUNT = 2;

    @NonNull
    private final List<Fragment> fragments;

    public ViewPagerAdapter(@NonNull FragmentManager fragmentManager) {
        super(fragmentManager);

        fragments = new ArrayList<>();
        fragments.add(DASHBOARD_FRAGMENT_ID, new DashboardFragment());
        fragments.add(FAVOURITES_FRAGMENT_ID, new FavouritesFragment());
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }
}
