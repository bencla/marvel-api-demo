package sk.danibenc.marvelapidemo.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import sk.danibenc.marvelapidemo.R;
import sk.danibenc.marvelapidemo.fragment.DashboardFragment;
import sk.danibenc.marvelapidemo.model.entity.Comic;
import sk.danibenc.marvelapidemo.view.ComicCard;

/**
 * Adapter for a recycler view used in the {@link DashboardFragment}
 *
 * @author bencurova
 */
public class ComicAdapter extends RecyclerView.Adapter<ComicAdapter.ViewHolder> {

    @NonNull
    private List<Comic> mComics = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.comic_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.card.setData(mComics.get(position));
    }

    @Override
    public int getItemCount() {
        return mComics.size();
    }

    /**
     * Sets comics.
     *
     * @param comics data to be set.
     */
    public void setComics(@NonNull List<Comic> comics) {
        mComics = comics;

        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final ComicCard card;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            card = itemView.findViewById(R.id.card);
        }
    }
}
