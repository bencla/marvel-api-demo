package sk.danibenc.marvelapidemo.util;

import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import sk.danibenc.marvelapidemo.R;

/**
 * Utils for dialog creation.
 *
 * @author bencurova
 */
public class DialogUtils {

    /**
     * Creates simple confirm dialog with message and action when positive button is clicked.
     *
     * @param message  message to be shown.
     * @param listener defines action to happen after positive click.
     * @param context  context
     * @return simple confirm alert dialog.
     */
    @NonNull
    public static AlertDialog getConfirmDialog(@NonNull String message, @NonNull DialogInterface.OnClickListener listener, @NonNull Context context) {
        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(R.string.ok, listener)
                .setNegativeButton(R.string.cancel, null)
                .create();
    }
}
