package sk.danibenc.marvelapidemo.util;

import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Utils for picasso library.
 *
 * @author bencurova
 */
public class PicassoUtils {

    public static void loadImage(@NonNull String url, @NonNull ImageView imageView) {
        loadImage(url, imageView, null);
    }

    public static void loadImage(@NonNull String url, @NonNull ImageView imageView, @Nullable Callback callback) {
        loadImage(url, imageView, 2000, 1000, callback);
    }

    public static void loadImage(@NonNull String url, @NonNull ImageView imageView, int width, int height, @Nullable Callback callback) {
        Picasso.get()
                .load(url)
                .resize(width, height)
                .centerCrop()
                .into(imageView, callback);
    }
}
