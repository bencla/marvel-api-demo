package sk.danibenc.marvelapidemo.util;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import androidx.annotation.NonNull;
import sk.danibenc.marvelapidemo.BuildConfig;

import static sk.danibenc.marvelapidemo.logging.LoggingTags.RETROFIT;

/**
 * Utils for retrofit.
 *
 * @author bencurova
 */
public class RetrofitUtils {

    /**
     * Creates a hash used when calling Marvel API.
     *
     * @param timestamp current time.
     * @return hash based on the timestamp and API key.
     */
    @NonNull
    public static String getHash(@NonNull String timestamp) {
        try {
            final String value = timestamp + getPrivateApiKey() + getPublicApiKey();
            final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final byte[] messageDigestBytes = messageDigest.digest(value.getBytes());

            final StringBuilder stringBuilder = new StringBuilder();
            for (byte messageDigestByte : messageDigestBytes) {
                stringBuilder.append(Integer.toHexString((messageDigestByte & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(RETROFIT, "Could not generate the hash", e);
        }
        return "";
    }

    /**
     * Gets Marvel API private key.
     *
     * @return API private key.
     */
    @NonNull
    public static String getPrivateApiKey() {
        return BuildConfig.MARVEL_PRIVATE_API_KEY;
    }

    /**
     * Gets Marvel API public key.
     *
     * @return API public key.
     */
    @NonNull
    public static String getPublicApiKey() {
        return BuildConfig.MARVEL_PUBLIC_API_KEY;
    }
}
