package sk.danibenc.marvelapidemo.db;

import android.content.Context;
import android.os.AsyncTask;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import static sk.danibenc.marvelapidemo.db.ApplicationDatabase.DATABASE_NAME;

/**
 * Creates the {@link ApplicationDatabase} asynchronously, exposing a LiveData object to notify of creation.
 *
 * @author bencurova
 */
@Singleton
public class DatabaseCreator {

    @NonNull
    private final Context mContext;

    private final AtomicBoolean mInitializing = new AtomicBoolean(true);

    private final MutableLiveData<ApplicationDatabase> mLiveDatabase = new MutableLiveData<>();

    @Inject
    DatabaseCreator(@NonNull Context context) {
        mContext = context;
    }

    /**
     * Creates the database.
     */
    public void createDatabase() {
        if (!mInitializing.compareAndSet(true, false)) {
            return; // Already initializing
        }

        new InitializeDatabaseTask(mLiveDatabase).execute(mContext);
    }

    /**
     * Gets database instance wrapped in {@link androidx.lifecycle.LiveData} which actions should lead to update on main thread.
     *
     * @return mutable live data with database instance
     */
    @NonNull
    public MutableLiveData<ApplicationDatabase> getLiveDatabase() {
        return mLiveDatabase;
    }

    static class InitializeDatabaseTask extends AsyncTask<Context, Void, ApplicationDatabase> {

        @NonNull
        private final MutableLiveData<ApplicationDatabase> mLiveDatabase;

        InitializeDatabaseTask(@NonNull MutableLiveData<ApplicationDatabase> liveDatabase) {
            mLiveDatabase = liveDatabase;
        }

        @Override
        protected ApplicationDatabase doInBackground(Context... params) {
            final Context context = params[0].getApplicationContext();

            return Room.databaseBuilder(context, ApplicationDatabase.class, DATABASE_NAME).build();
        }

        @Override
        protected void onPostExecute(ApplicationDatabase database) {
            // Now on the main thread, notify observers that the db is created and ready.
            mLiveDatabase.setValue(database);
        }
    }
}
