package sk.danibenc.marvelapidemo.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import sk.danibenc.marvelapidemo.model.entity.Comic;
import sk.danibenc.marvelapidemo.model.dao.ComicDao;
import sk.danibenc.marvelapidemo.model.entity.Thumbnail;
import sk.danibenc.marvelapidemo.model.entity.converter.RoomConverters;

/**
 * Class representing application database.
 *
 * @author bencurova
 */
@Database(entities = {Comic.class, Thumbnail.class}, version = 1)
@TypeConverters({RoomConverters.class})
public abstract class ApplicationDatabase extends RoomDatabase {

    static final String DATABASE_NAME = "marvel_database";

    /**
     * Gets Comic DAO object.
     * @see ComicDao
     *
     * @return ComicDao
     */
    public abstract ComicDao getComicDao();
}
