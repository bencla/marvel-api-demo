package sk.danibenc.marvelapidemo.preferences;

/**
 * Holds constants used in shared preferences.
 *
 * @author bencurova
 */
public class PreferencesConstants {

    // Keys
    public static final String KEY_USE_DARK_MODE = "use_dark_mode";

    // Default values
    public static final boolean DEFAULT_USE_DARK_MODE = false;
}
