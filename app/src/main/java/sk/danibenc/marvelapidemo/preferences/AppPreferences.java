package sk.danibenc.marvelapidemo.preferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;

import static sk.danibenc.marvelapidemo.preferences.PreferencesConstants.DEFAULT_USE_DARK_MODE;
import static sk.danibenc.marvelapidemo.preferences.PreferencesConstants.KEY_USE_DARK_MODE;


/**
 * Holds application preferences.
 *
 * @author bencurova
 */
@Singleton
public class AppPreferences {

    @NonNull
    private final Preferences mPreferences;

    @Inject
    public AppPreferences(@NonNull Preferences preferences) {
        mPreferences = preferences;
    }

    /**
     * Sets whether the app should use the dark mode.
     *
     * @param enabled value to be set.
     */
    public void setDarkThemeEnabled(boolean enabled) {
        mPreferences.setBoolean(KEY_USE_DARK_MODE, enabled);
    }

    /**
     * Gets whether app should use the dark mode.
     *
     * @return True if app should use the dark mode, false otherwise.
     */
    public boolean isDarkModeEnabled() {
        return mPreferences.getBoolean(KEY_USE_DARK_MODE, DEFAULT_USE_DARK_MODE);
    }
}
