package sk.danibenc.marvelapidemo.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;

/**
 * Wrapper for shared preferences.
 *
 * @author bencurova
 */
@Singleton
public class SharedPreferencesWrapper implements Preferences {

    @NonNull
    private final SharedPreferences mPreferences;

    @Inject
    public SharedPreferencesWrapper(@NonNull Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    @Override
    public String getString(@NonNull String key, @NonNull String defValue) {
        return mPreferences.getString(key, defValue);
    }

    @Override
    public boolean getBoolean(@NonNull String key, boolean defValue) {
        return mPreferences.getBoolean(key, defValue);
    }

    @Override
    public int getInt(@NonNull String key, int defValue) {
        return mPreferences.getInt(key, defValue);
    }

    @Override
    public void setString(@NonNull String key, @NonNull String value) {
        mPreferences.edit().putString(key, value).apply();
    }

    @Override
    public void setBoolean(@NonNull String key, boolean value) {
        mPreferences.edit().putBoolean(key, value).apply();
    }

    @Override
    public void setInt(@NonNull String key, int value) {
        mPreferences.edit().putInt(key, value).apply();
    }
}
