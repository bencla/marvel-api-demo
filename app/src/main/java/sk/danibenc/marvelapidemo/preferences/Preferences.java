package sk.danibenc.marvelapidemo.preferences;

import androidx.annotation.NonNull;

/**
 * Preferences interface.
 *
 * @author bencurova
 */
public interface Preferences {

    /**
     * Returns string value of the searched preference.
     *
     * @param key key of the preference.
     * @param defValue default value returned if no value found for the key.
     */
    @NonNull
    String getString(@NonNull String key, @NonNull String defValue);

    /**
     * Returns boolean value of the searched preference.
     *
     * @param key key of the preference.
     * @param defValue default value returned if no value found for the key.
     */
    boolean getBoolean(@NonNull String key, boolean defValue);

    /**
     * Returns int value of the searched preference.
     *
     * @param key key of the preference.
     * @param defValue default value returned if no value found for the key.
     */
    int getInt(@NonNull String key, int defValue);

    /**
     * Sets string value to the preference.
     *
     * @param key key of the preference.
     * @param value value to be set.
     */
    void setString(@NonNull String key, @NonNull String value);

    /**
     * Sets boolean value to the preference.
     *
     * @param key key of the preference.
     * @param value value to be set.
     */
    void setBoolean(@NonNull String key, boolean value);

    /**
     * Sets int value to the preference.
     *
     * @param key key of the preference.
     * @param value value to be set.
     */
    void setInt(@NonNull String key, int value);
}