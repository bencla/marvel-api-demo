package sk.danibenc.marvelapidemo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import sk.danibenc.marvelapidemo.R;
import sk.danibenc.marvelapidemo.activity.base.BaseToolbarActivity;
import sk.danibenc.marvelapidemo.fragment.PreferenceFragment;

/**
 * Settings activity.
 *
 * @author bencurova
 */
public class PreferenceActivity extends BaseToolbarActivity {

    /**
     * Starts the activity.
     *
     * @param context start point.
     */
    public static void startActivity(@NonNull Context context) {
        context.startActivity(new Intent(context, PreferenceActivity.class));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        showPreferenceFragment();
    }

    private void showPreferenceFragment() {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, new PreferenceFragment());
        transaction.commit();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_settings;
    }

    @Override
    @NonNull
    protected String getToolbarTitle() {
        return getString(R.string.preferences_title);
    }
}
