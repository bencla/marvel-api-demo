package sk.danibenc.marvelapidemo.activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import sk.danibenc.marvelapidemo.R;
import sk.danibenc.marvelapidemo.activity.base.BaseActivity;
import sk.danibenc.marvelapidemo.adapter.ViewPagerAdapter;
import sk.danibenc.marvelapidemo.view.SwipeSettableViewPager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import static androidx.core.view.GravityCompat.START;
import static sk.danibenc.marvelapidemo.adapter.ViewPagerAdapter.DASHBOARD_FRAGMENT_ID;
import static sk.danibenc.marvelapidemo.adapter.ViewPagerAdapter.FAVOURITES_FRAGMENT_ID;

/**
 * Main project activity.
 *
 * @author bencurova
 */
public class MainActivity extends BaseActivity {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView vBottomNavigation;

    @BindView(R.id.drawer_layout)
    DrawerLayout vDrawerLayout;

    @BindView(R.id.drawer_navigation)
    NavigationView vDrawerNavigation;

    @BindView(R.id.viewpager)
    SwipeSettableViewPager vViewPager;

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    /**
     * Starts the activity.
     *
     * @param context start point.
     */
    public static void startActivity(@NonNull Context context) {
        final Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        registerUnbinder(ButterKnife.bind(this));

        prepareNavigation();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void prepareNavigation() {
        vToolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(vToolbar);

        vViewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        vViewPager.enableSwipe(false);
        vViewPager.setCurrentItem(0);

        vBottomNavigation.setOnNavigationItemSelectedListener(this::onBottomNavigationItemSelected);
        vDrawerNavigation.setNavigationItemSelectedListener(this::onDrawerNavigationItemSelected);
    }

    private boolean onBottomNavigationItemSelected(@NonNull MenuItem menuItem) {
        menuItem.setChecked(true);
        final int menuItemId = menuItem.getItemId();
        vViewPager.setCurrentItem(getFragmentIdFromMenuId(menuItemId));
        vDrawerNavigation.setCheckedItem(menuItemId);
        return true;
    }

    private boolean onDrawerNavigationItemSelected(@NonNull MenuItem menuItem) {
        menuItem.setChecked(true);
        vDrawerLayout.closeDrawers();
        if (menuItem.getItemId() == R.id.settings) {
            PreferenceActivity.startActivity(this);
            return true;
        }
        final int menuItemId = menuItem.getItemId();
        vViewPager.setCurrentItem(getFragmentIdFromMenuId(menuItemId));
        vBottomNavigation.setSelectedItemId(menuItemId);
        return true;
    }

    private int getFragmentIdFromMenuId(@IdRes int menuItemId) {
        switch (menuItemId) {
            case R.id.favourites:
                return FAVOURITES_FRAGMENT_ID;
            case R.id.dashboard:
            default:
                return DASHBOARD_FRAGMENT_ID;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                vDrawerLayout.openDrawer(START);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    @Override
    public void onBackPressed() {
        if (vDrawerLayout.isDrawerOpen(START)) {
            vDrawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }
}
