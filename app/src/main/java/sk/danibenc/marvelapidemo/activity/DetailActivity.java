package sk.danibenc.marvelapidemo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import sk.danibenc.marvelapidemo.R;
import sk.danibenc.marvelapidemo.activity.base.BaseToolbarActivity;
import sk.danibenc.marvelapidemo.model.entity.Comic;

import static sk.danibenc.marvelapidemo.util.PicassoUtils.loadImage;

/**
 * Activity used to show detail of the comic series based on the comic id passed when
 * the activity is started.
 *
 * @author bencurova
 */
public class DetailActivity extends BaseToolbarActivity {

    private static final String INTENT_EXTRA_COMIC = "comic";

    private Comic mComic;

    /**
     * Starts the activity.
     *
     * @param context start point.
     * @param comic   chosen comic.
     */
    public static void startActivity(@NonNull Context context, @NonNull Comic comic) {
        final Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(INTENT_EXTRA_COMIC, comic);
        context.startActivity(intent);
    }

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @BindView(R.id.header_background)
    ImageView vHeaderBackground;

    @BindView(R.id.comic_id)
    TextView vId;

    @BindView(R.id.desc)
    TextView vDesc;

    @BindView(R.id.characters)
    TextView vCharacters;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        handleIntent();

        super.onCreate(savedInstanceState);

        registerUnbinder(ButterKnife.bind(this));

        initContent();
    }

    private void handleIntent() {
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mComic = (Comic) extras.get(INTENT_EXTRA_COMIC);
        }
    }

    private void initContent() {
        loadImage(mComic.getThumbnailUrl(), vHeaderBackground);

        final String description = mComic.getDescription();
        vDesc.setText(description == null ? getString(R.string.desc_unavailable) : description);
        vId.setText(String.valueOf("#" + mComic.getId()));
        vCharacters.setText(mComic.getCharacters().getAvailable() == 0 ? getString(R.string.characters_unavailable) : mComic.getCharacterNames());
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_detail;
    }

    @Override
    @NonNull
    protected String getToolbarTitle() {
        return mComic.getTitle();
    }
}
