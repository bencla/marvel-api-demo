package sk.danibenc.marvelapidemo.activity.base;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import sk.danibenc.marvelapidemo.R;

/**
 * Base activity using the toolbar.
 *
 * @author bencurova
 */
public abstract class BaseToolbarActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar vToolbar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutRes());

        registerUnbinder(ButterKnife.bind(this));

        setupToolbar();
    }

    private void setupToolbar() {
        setSupportActionBar(vToolbar);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getToolbarTitle());
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        }
    }

    /**
     * Gets layout resource.
     *
     * @return layout res.
     */
    @LayoutRes
    protected abstract int getLayoutRes();

    /**
     * Gets title string used in the toolbar.
     *
     * @return activity title.
     */
    @NonNull
    protected abstract String getToolbarTitle();
}
