package sk.danibenc.marvelapidemo.activity.base;

import android.os.Bundle;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.Unbinder;
import sk.danibenc.marvelapidemo.MarvelApplication;
import sk.danibenc.marvelapidemo.R;
import sk.danibenc.marvelapidemo.preferences.AppPreferences;

/**
 * Base activity.
 *
 * @author bencurova
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    AppPreferences mPreferences;

    @Nullable
    private Unbinder mUnbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injectSelf();

        setTheme(getThemeRes());
    }

    protected void injectSelf() {
        MarvelApplication.getApplication(this).getApplicationComponent().injectBaseActivity(this);
    }

    @StyleRes
    protected int getThemeRes() {
        return mPreferences.isDarkModeEnabled() ? R.style.AppTheme_Dark : R.style.AppTheme_Light;
    }

    protected void registerUnbinder(@NonNull Unbinder unbinder) {
        mUnbinder = unbinder;
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}
