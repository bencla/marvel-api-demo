package sk.danibenc.marvelapidemo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

/**
 * Custom view pager that allows to disable user swipe to change pages.
 *
 * @author bencurova
 */
public class SwipeSettableViewPager extends ViewPager {

    private boolean mSwipeEnabled;

    public SwipeSettableViewPager(@NonNull Context context) {
        this(context, null);
    }

    public SwipeSettableViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return mSwipeEnabled && super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        performClick();
        return mSwipeEnabled && super.onTouchEvent(ev);
    }

    @Override
    public boolean performClick() {
        return mSwipeEnabled && super.performClick();
    }

    /**
     * Sets whether the view pager allows swipe or not.
     *
     * @param enable value to be set.
     */
    public void enableSwipe(boolean enable) {
        this.mSwipeEnabled = enable;
    }
}
