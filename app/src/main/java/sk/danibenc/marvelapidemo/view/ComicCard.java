package sk.danibenc.marvelapidemo.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;

import javax.inject.Inject;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sk.danibenc.marvelapidemo.MarvelApplication;
import sk.danibenc.marvelapidemo.R;
import sk.danibenc.marvelapidemo.activity.DetailActivity;
import sk.danibenc.marvelapidemo.model.entity.Comic;
import sk.danibenc.marvelapidemo.model.repository.base.ComicRepository;

import static sk.danibenc.marvelapidemo.logging.LoggingTags.PICASSO;
import static sk.danibenc.marvelapidemo.logging.LoggingTags.UI;
import static sk.danibenc.marvelapidemo.util.PicassoUtils.loadImage;

/**
 * Card used to display basic info about comic series.
 *
 * @author bencurova
 */
public class ComicCard extends CardView {

    @Inject
    ComicRepository mComicRepository;

    private Comic mComic;

    @BindView(R.id.comic_name)
    TextView vName;

    @BindView(R.id.comic_id)
    TextView vId;

    @BindView(R.id.comic_image)
    ImageView vImage;

    @BindView(R.id.progress_bar)
    ProgressBar vProgressBar;

    @BindView(R.id.like)
    ImageView vLike;

    @BindView(R.id.comic_options)
    ImageView vOptions;

    public ComicCard(@NonNull Context context) {
        this(context, null);
    }

    public ComicCard(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ComicCard(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);
    }

    private void init(@NonNull Context context) {
        final View view = LayoutInflater.from(context).inflate(R.layout.view_comic_card_item, this, true);

        ButterKnife.bind(this, view);

        injectSelf(context);
    }

    private void injectSelf(@NonNull Context context) {
        MarvelApplication.getApplication(context).getApplicationComponent().injectComicCard(this);
    }

    /**
     * Set data in the comic card.
     *
     * @param comic comic to be represented on card.
     */
    public void setData(@NonNull Comic comic) {
        mComic = comic;

        vName.setText(comic.getTitle());
        vId.setText(String.valueOf("#" + mComic.getId()));

        loadImage(comic.getThumbnailUrl(), vImage, getPicassoCallback());

        initHeartDrawable();
    }

    private void initHeartDrawable() {
        mComicRepository.getComic(mComic.getId()).observeForever(comic -> {
            vLike.setImageDrawable(getResources().getDrawable(comic == null ? R.drawable.ic_favorite_border : R.drawable.ic_favorite));
        });
    }

    @NonNull
    private Callback getPicassoCallback() {
        return new Callback() {
            @Override
            public void onSuccess() {
                vProgressBar.setVisibility(GONE);
            }

            @Override
            public void onError(Exception e) {
                vProgressBar.setVisibility(GONE);
                vImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_error));
                Log.e(PICASSO, "Failed to load an image: ", e);
            }
        };
    }

    @OnClick(R.id.comic_image)
    public void onImageClick(@NonNull View view) {
        showComicDetail(view);
    }

    @OnClick(R.id.detail)
    public void onDetailButtonClick(@NonNull View view) {
        showComicDetail(view);
    }

    private void showComicDetail(@NonNull View view) {
        if(mComic == null) {
            Log.i(UI, "No action - comic not loaded yet");
            return;
        }
        DetailActivity.startActivity(view.getContext(), mComic);
    }

    @OnClick(R.id.like)
    public void onLikeClick() {
        likeComicSeries();
    }

    @OnClick(R.id.comic_options)
    public void onComicOptionsClick(@NonNull View view) {
        final PopupMenu popupMenu = new PopupMenu(view.getContext(), vOptions);
        popupMenu.getMenuInflater().inflate(R.menu.comic_card_overflow_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(item -> onPopupMenuItemClick(item.getItemId()));
        popupMenu.show();
    }

    private boolean onPopupMenuItemClick(@IdRes int itemId) {
        switch (itemId) {
            case R.id.popup_show_in_browser:
                showComicDetailInBrowser();
                break;
            case R.id.popup_like_series:
                likeComicSeries();
                break;
            default:
                Log.i(UI, "Unknown menu item");
        }
        return false;
    }

    private void likeComicSeries() {
        if (mComic.isFavorite()) {
            mComic.setFavorite(false);
            mComicRepository.deleteComic(mComic);
        } else {
            mComic.setFavorite(true);
            mComicRepository.insertComic(mComic);
        }
    }

    private void showComicDetailInBrowser() {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mComic.getDetailUrl()));
        getContext().startActivity(intent);
    }
}
