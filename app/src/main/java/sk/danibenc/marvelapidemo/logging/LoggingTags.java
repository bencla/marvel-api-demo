package sk.danibenc.marvelapidemo.logging;

/**
 * Holds the logging tag used in the logger.
 *
 * @author bencurova
 */
public class LoggingTags {

    /**
     * Tag for retrofit picasso logs.
     */
    public static final String PICASSO = "picasso";

    /**
     * Tag for retrofit related logs.
     */
    public static final String RETROFIT = "retrofit";

    /**
     * Tag for UI related logs.
     */
    public static final String UI = "UI";
}
