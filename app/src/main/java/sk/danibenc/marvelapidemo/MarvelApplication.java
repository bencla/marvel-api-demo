package sk.danibenc.marvelapidemo;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;

import androidx.annotation.NonNull;
import sk.danibenc.marvelapidemo.dagger.ApplicationComponent;
import sk.danibenc.marvelapidemo.dagger.DaggerApplicationComponent;
import sk.danibenc.marvelapidemo.dagger.module.ApplicationModule;

/**
 * Main application class.
 *
 * @author bencurova
 */
public class MarvelApplication extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        initDependencyInjection();
    }

    private void initDependencyInjection() {
        mApplicationComponent = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        injectSelf();

        Stetho.initializeWithDefaults(this);
    }

    private void injectSelf() {
        mApplicationComponent.injectApplication(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }

    public static MarvelApplication getApplication(@NonNull Context context) {
        return (MarvelApplication) context.getApplicationContext();
    }
}
