package sk.danibenc.marvelapidemo.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.RoomWarnings;

/**
 * Data class representing a Marvel comic.
 *
 * @author bencurova
 */
@Entity
@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
public class Comic implements Parcelable {

    private static final String THUMBNAIL_URL_APPENDIX = "/standard_fantastic.";
    private static final String URL_DETAIL = "detail";

    @NonNull
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @NonNull
        public Comic createFromParcel(Parcel in) {
            return new Comic(in);
        }

        @NonNull
        public Comic[] newArray(int size) {
            return new Comic[size];
        }
    };

    @PrimaryKey
    private int id;

    @ColumnInfo
    private String title;

    @ColumnInfo
    private String description;

    @Embedded
    private Thumbnail thumbnail;

    @ColumnInfo
    private List<Url> urls;

    @Embedded
    private Characters characters;

    @ColumnInfo
    private boolean isFavorite;

    public Comic(String title, int id, Thumbnail thumbnail, List<Url> urls, String description, Characters characters) {
        this.title = title;
        this.thumbnail = thumbnail;
        this.id = id;
        this.urls = urls;
        this.description = description;
        this.characters = characters;
    }

    private Comic(@NonNull Parcel parcel) {
        id = parcel.readInt();
        title = parcel.readString();
        description = parcel.readString();
        thumbnail = parcel.readParcelable(Thumbnail.class.getClassLoader());
        urls = new ArrayList<>();
        parcel.readList(urls, Url.class.getClassLoader());
        characters = parcel.readParcelable(Characters.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    /**
     * Gets thumbnail url.
     * More about images from Marvel API: https://developer.marvel.com/documentation/images
     *
     * @return thumbnail url.
     */
    public String getThumbnailUrl() {
        return thumbnail.getPath() + THUMBNAIL_URL_APPENDIX + thumbnail.getExtension();
    }

    public List<Url> getUrls() {
        return urls;
    }

    public Characters getCharacters() {
        return characters;
    }

    /**
     * Gets comic series detail URL.
     *
     * @return detail URL.
     */
    @Nullable
    public String getDetailUrl() {
        for (Url url : urls) {
            if (url.getType().equals(URL_DETAIL)) {
                return url.getUrl();
            }
        }
        return null;
    }

    /**
     * Gets list of all character names.
     *
     * @return character names.
     */
    @NonNull
    public String getCharacterNames() {
        final StringBuilder stringBuilder = new StringBuilder();
        for (String name : getCharacters().getCharacterNames()) {
            stringBuilder.append(name).append(", ");
        }
        return stringBuilder.toString();
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeParcelable(thumbnail, flags);
        dest.writeList(urls);
        dest.writeParcelable(characters, flags);
    }

    @NonNull
    @Override
    public String toString() {
        return "Comic{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", thumbnail=" + thumbnail +
                ", urls=" + urls +
                ", characters=" + characters +
                ", isFavorite=" + isFavorite +
                '}';
    }
}
