package sk.danibenc.marvelapidemo.model.entity.converter;

import sk.danibenc.marvelapidemo.model.entity.Character;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;
import sk.danibenc.marvelapidemo.model.entity.Url;

/**
 * Converter helper methods for Room.
 *
 * @author bencurova
 */
public class RoomConverters {

    /**
     * Converts json string to list of {@link Character}.
     *
     * @param value representation of the character list.
     * @return list of characters.
     */
    @TypeConverter
    public static List<Character> charactersFromString(@NonNull String value) {
        final Type listType = new TypeToken<List<Character>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    /**
     * Converts list of {@link Character} to json.
     *
     * @param list list of characters.
     * @return json representation of the character list.
     */
    @TypeConverter
    public static String fromCharacterList(List<Character> list) {
        return new Gson().toJson(list);
    }

    /**
     * Converts json string to list of {@link Url}.
     *
     * @param value representation of the url list.
     * @return list of urls.
     */
    @TypeConverter
    public static List<Url> urlsFromString(@NonNull String value) {
        final Type listType = new TypeToken<List<Url>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    /**
     * Converts list of {@link Url} to json.
     *
     * @param list list of urls.
     * @return json representation of the url list.
     */
    @TypeConverter
    public static String fromUrlList(List<Url> list) {
        return new Gson().toJson(list);
    }
}
