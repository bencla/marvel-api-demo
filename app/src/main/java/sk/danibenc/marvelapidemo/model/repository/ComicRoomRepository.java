package sk.danibenc.marvelapidemo.model.repository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import sk.danibenc.marvelapidemo.db.DatabaseCreator;
import sk.danibenc.marvelapidemo.model.entity.Comic;
import sk.danibenc.marvelapidemo.model.repository.base.ApplicationRepository;
import sk.danibenc.marvelapidemo.model.repository.base.ComicRepository;
import sk.danibenc.marvelapidemo.retrofit.ComicDataLoader;

/**
 * @author bencurova
 * @see ComicRepository
 */
@Singleton
public class ComicRoomRepository extends ApplicationRepository implements ComicRepository {

    @Inject
    public ComicRoomRepository(@NonNull DatabaseCreator databaseCreator, @NonNull ComicDataLoader comicDataLoader) {
        super(databaseCreator, comicDataLoader);
    }

    @Override
    public LiveData<List<Comic>> getComics() {
        return getComicDataLoader().getData();
    }

    @Override
    public LiveData<Comic> getComic(int id) {
        return Transformations.switchMap(getLiveDatabase(),
                applicationDatabase -> applicationDatabase.getComicDao().getComic(id));
    }

    @Override
    public LiveData<List<Comic>> getFavouriteComics() {
        return Transformations.switchMap(getLiveDatabase(),
                applicationDatabase -> applicationDatabase.getComicDao().getFavouriteComics());
    }

    @Override
    public void insertComic(Comic comic) {
        waitForDatabaseInMainThread(new AsyncDatabaseRunnable() {
            @Override
            public void run() {
                getDatabase().getComicDao().insertComic(comic);
            }
        });
    }

    @Override
    public void deleteComic(Comic comic) {
        waitForDatabaseInMainThread(new AsyncDatabaseRunnable() {
            @Override
            public void run() {
                getDatabase().getComicDao().deleteComic(comic);
            }
        });
    }

    @Override
    public void fetchNewComics() {
        getComicDataLoader().fetchNewComics();
    }
}
