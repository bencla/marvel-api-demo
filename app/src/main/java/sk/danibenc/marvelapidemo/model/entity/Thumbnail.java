package sk.danibenc.marvelapidemo.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Data class representing thumbnail of the comic series.
 *
 * @author bencurova
 */
@Entity
public class Thumbnail implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int thumbnailId;

    @ColumnInfo
    private String path;

    @ColumnInfo
    private String extension;

    public Thumbnail(String path, String extension) {
        this.path = path;
        this.extension = extension;
    }

    private Thumbnail(Parcel in) {
        path = in.readString();
        extension = in.readString();
    }

    public static final Creator<Thumbnail> CREATOR = new Creator<Thumbnail>() {
        @Override
        public Thumbnail createFromParcel(Parcel in) {
            return new Thumbnail(in);
        }

        @Override
        public Thumbnail[] newArray(int size) {
            return new Thumbnail[size];
        }
    };

    public String getPath() {
        return path;
    }

    public String getExtension() {
        return extension;
    }

    public int getThumbnailId() {
        return thumbnailId;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setThumbnailId(int thumbnailId) {
        this.thumbnailId = thumbnailId;
    }

    @Override
    @NonNull
    public String toString() {
        return "Thumbnail{" +
                "thumbnailId=" + thumbnailId +
                ", path='" + path + '\'' +
                ", extension='" + extension + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
        dest.writeString(extension);
    }
}
