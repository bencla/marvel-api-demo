package sk.danibenc.marvelapidemo.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Data class representing URL.
 *
 * @author bencurova
 */
@Entity
public class Url implements Parcelable {

    @ColumnInfo
    private String url;

    @ColumnInfo
    private String type;

    private Url(Parcel in) {
        url = in.readString();
        type = in.readString();
    }

    String getUrl() {
        return url;
    }

    String getType() {
        return type;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Url> CREATOR = new Creator<Url>() {
        @Override
        public Url createFromParcel(Parcel in) {
            return new Url(in);
        }

        @Override
        public Url[] newArray(int size) {
            return new Url[size];
        }
    };
}
