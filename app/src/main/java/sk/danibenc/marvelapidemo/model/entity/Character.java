package sk.danibenc.marvelapidemo.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Data class representing a character.
 *
 * @author bencurova
 */
public class Character implements Parcelable {

    private String name;

    private Character(Parcel in) {
        name = in.readString();
    }

    public static final Creator<Character> CREATOR = new Creator<Character>() {
        @Override
        public Character createFromParcel(Parcel in) {
            return new Character(in);
        }

        @Override
        public Character[] newArray(int size) {
            return new Character[size];
        }
    };

    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }
}
