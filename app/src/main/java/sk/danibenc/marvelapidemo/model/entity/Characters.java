package sk.danibenc.marvelapidemo.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Represents items entity in the response.
 *
 * @author bencurova
 */
@Entity
public class Characters implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int charactersId;

    @ColumnInfo
    private int available;

    @ColumnInfo
    private List<Character> items;

    public Characters(int available, List<Character> items) {
        this.available = available;
        this.items = items;
    }

    private Characters(Parcel in) {
        available = in.readInt();
        items = in.createTypedArrayList(Character.CREATOR);
    }

    public static final Creator<Characters> CREATOR = new Creator<Characters>() {
        @Override
        public Characters createFromParcel(Parcel in) {
            return new Characters(in);
        }

        @Override
        public Characters[] newArray(int size) {
            return new Characters[size];
        }
    };

    /**
     * Gets list of all character names.
     *
     * @return character names.
     */
    @NonNull
    List<String> getCharacterNames() {
        final List<Character> characters = getItems();
        if (characters == null || characters.isEmpty()) {
            return new ArrayList<>();
        }

        final List<String> names = new ArrayList<>();
        for (Character character : characters) {
            names.add(character.getName());
        }
        return names;
    }

    public int getCharactersId() {
        return charactersId;
    }

    public int getAvailable() {
        return available;
    }

    public List<Character> getItems() {
        return items;
    }

    public void setCharactersId(int charactersId) {
        this.charactersId = charactersId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(available);
        dest.writeTypedList(items);
    }
}
