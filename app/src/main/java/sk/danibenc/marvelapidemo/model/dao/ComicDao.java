package sk.danibenc.marvelapidemo.model.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import sk.danibenc.marvelapidemo.model.entity.Comic;

import static androidx.room.OnConflictStrategy.REPLACE;

/**
 * Data access object for {@link Comic}.
 *
 * @author bencurova
 */
@Dao
public interface ComicDao {

    @Query("SELECT * FROM Comic WHERE id = :id")
    LiveData<Comic> getComic(int id);

    @Query("SELECT * FROM Comic")
    LiveData<List<Comic>> getFavouriteComics();

    @Insert(onConflict = REPLACE)
    void insertComic(Comic comic);

    @Delete
    void deleteComic(Comic comic);
}
