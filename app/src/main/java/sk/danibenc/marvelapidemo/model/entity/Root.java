package sk.danibenc.marvelapidemo.model.entity;

/**
 * Represents the root of Marvel API call response.
 *
 * @author bencurova
 */
public class Root {

    private Data data;

    public Root(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }
}
