package sk.danibenc.marvelapidemo.model.entity;

import java.util.List;

import androidx.annotation.NonNull;

/**
 * Represents data element of Marvel API call.
 *
 * @author bencurova
 */
public class Data {

    private List<Comic> results;

    public Data(List<Comic> results) {
        this.results = results;
    }

    public List<Comic> getComics() {
        return results;
    }

    @NonNull
    @Override
    public String toString() {
        return "Data{" +
                "results=" + results +
                '}';
    }
}
