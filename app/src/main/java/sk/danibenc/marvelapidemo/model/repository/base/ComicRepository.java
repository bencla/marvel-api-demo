package sk.danibenc.marvelapidemo.model.repository.base;

import java.util.List;

import androidx.lifecycle.LiveData;
import sk.danibenc.marvelapidemo.model.entity.Comic;

/**
 * Comic repository interface.
 *
 * @author bencurova
 */
public interface ComicRepository {

    /**
     * Gets all comics in database.
     */
    LiveData<List<Comic>> getComics();

    /**
     * Gets a specific comic based on comic's num.
     *
     * @param id comic's id.
     */
    LiveData<Comic> getComic(int id);

    /**
     * Gets user's favourite comics.
     */
    LiveData<List<Comic>> getFavouriteComics();

    /**
     * Inserts new comic.
     *
     * @param comic comic to be inserted.
     */
    void insertComic(Comic comic);

    /**
     * Deletes a specific comic from the database.
     *
     * @param comic comic to be deleted.
     */
    void deleteComic(Comic comic);

    /**
     * Loads new comic data.
     */
    void fetchNewComics();
}
