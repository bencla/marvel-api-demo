package sk.danibenc.marvelapidemo.model.viewmodel;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import dagger.Module;
import sk.danibenc.marvelapidemo.model.entity.Comic;
import sk.danibenc.marvelapidemo.model.repository.base.ComicRepository;

/**
 * View-model for favourite comics.
 *
 * @author bencurova
 */
@Module
public class ComicViewModel extends ViewModel {

    @NonNull
    private final ComicRepository mRepository;

    @Inject
    public ComicViewModel(@NonNull ComicRepository repository) {
        mRepository = repository;
    }

    /**
     * Gets favourite comics.
     *
     * @return favourite comics.
     */
    @NonNull
    public LiveData<List<Comic>> getFavouriteComicSeries() {
        return mRepository.getFavouriteComics();
    }

    /**
     * Gets all comics.
     *
     * @return all comics.
     */
    @NonNull
    public LiveData<List<Comic>> getComicSeries() {
        return mRepository.getComics();
    }

    /**
     * Fetches new comics.
     */
    public void fetchNewComics() {
        mRepository.fetchNewComics();
    }
}
