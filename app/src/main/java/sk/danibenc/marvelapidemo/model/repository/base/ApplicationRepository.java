package sk.danibenc.marvelapidemo.model.repository.base;

import android.os.Handler;
import android.os.HandlerThread;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import sk.danibenc.marvelapidemo.db.ApplicationDatabase;
import sk.danibenc.marvelapidemo.db.DatabaseCreator;
import sk.danibenc.marvelapidemo.retrofit.ComicDataLoader;

/**
 * Base application repository.
 *
 * @author bencurova
 */
public abstract class ApplicationRepository {

    @NonNull
    private final LiveData<ApplicationDatabase> mLiveDatabase;

    @NonNull
    private final ComicDataLoader mComicDataLoader;

    public ApplicationRepository(@NonNull DatabaseCreator databaseCreator, @NonNull ComicDataLoader comicDataLoader) {
        databaseCreator.createDatabase();
        mLiveDatabase = databaseCreator.getLiveDatabase();
        mComicDataLoader = comicDataLoader;
    }

    /**
     * Gets live database.
     */
    @NonNull
    protected LiveData<ApplicationDatabase> getLiveDatabase() {
        return mLiveDatabase;
    }

    /**
     * Gets data loader.
     */
    @NonNull
    protected ComicDataLoader getComicDataLoader() {
        return mComicDataLoader;
    }

    /**
     * Run this runnable after database is initialized on Main thread.
     *
     * @param databaseRunnable runnable to run
     */
    @MainThread
    protected void waitForDatabaseInMainThread(@NonNull final DatabaseRunnable databaseRunnable) {
        getLiveDatabase().observeForever(new Observer<ApplicationDatabase>() {
            @Override
            public void onChanged(@Nullable ApplicationDatabase applicationDatabase) {
                if (applicationDatabase == null) {
                    return;
                }

                getLiveDatabase().removeObserver(this);

                databaseRunnable.run(applicationDatabase);
            }
        });
    }

    public interface DatabaseRunnable {

        /**
         * Defines database action to run on the thread.
         */
        void run(@NonNull ApplicationDatabase applicationDatabase);
    }

    public static abstract class AsyncDatabaseRunnable implements DatabaseRunnable, Runnable {

        @Nullable
        private ApplicationDatabase mDatabase;

        @Override
        public void run(@NonNull ApplicationDatabase applicationDatabase) {
            mDatabase = applicationDatabase;
            final HandlerThread handlerThread = new HandlerThread("AsyncHandler");
            handlerThread.start();
            new Handler(handlerThread.getLooper()).post(this);
        }

        @NonNull
        protected ApplicationDatabase getDatabase() {
            if (mDatabase == null) {
                throw new IllegalStateException("Database is not yet initialized.");
            }
            return mDatabase;
        }
    }
}
