package sk.danibenc.marvelapidemo.retrofit;

import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import sk.danibenc.marvelapidemo.model.entity.Root;

/**
 * Defines retrofit endpoints.
 *
 * @author bencurova
 */
public interface MarvelComicsService {

    String MARVEL_ENDPOINT = "https://gateway.marvel.com";

    /**
     * Gets a Marvel comics service.
     *
     * @return Marvel comics service.
     */
    @NonNull
    static MarvelComicsService create() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(MARVEL_ENDPOINT)
                .build()
                .create(MarvelComicsService.class);
    }

    /**
     * Gets list of Marvel comic series.
     *
     * @param timestamp timestamp (or other long string which can change on a request-by-request basis).
     * @param key       public API key.
     * @param hash      a md5 digest of the ts parameter, your private key and your public key (e.g. md5(ts+privateKey+publicKey).
     * @param offset    skips the specified number of resources in the result set.
     * @return list of Marvel comic series.
     */
    @GET("/v1/public/series?orderBy=modified")
    Call<Root> getComicSeries(@Query("ts") @NonNull String timestamp, @Query("apikey") @NonNull String key,
                              @Query("hash") @NonNull String hash, @Query("offset") int offset);
}
