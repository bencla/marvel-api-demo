package sk.danibenc.marvelapidemo.retrofit;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sk.danibenc.marvelapidemo.model.entity.Comic;
import sk.danibenc.marvelapidemo.model.entity.Root;

import static sk.danibenc.marvelapidemo.logging.LoggingTags.RETROFIT;
import static sk.danibenc.marvelapidemo.util.RetrofitUtils.getPublicApiKey;
import static sk.danibenc.marvelapidemo.util.RetrofitUtils.getHash;

/**
 * Loads data via Marvel API using {@link MarvelComicsService} and then holds them.
 *
 * @author bencurova
 */
@Singleton
public class ComicDataLoader implements Callback<Root> {

    private static final int RANGE = 20;

    @NonNull
    private final MutableLiveData<List<Comic>> mData;

    private boolean mLoaded;
    private int mLastOffset = 0;

    @Inject
    public ComicDataLoader() {
        mData = new MutableLiveData<>();
    }

    /**
     * Gets comic data.
     *
     * @return comic data.
     */
    @NonNull
    public LiveData<List<Comic>> getData() {
        if (!mLoaded) {
            loadComicSeriesData(this, mLastOffset);
        }
        return mData;
    }

    /**
     * Fetches new comic data.
     */
    public void fetchNewComics() {
        loadComicSeriesData(this, mLastOffset);
    }

    /**
     * Loads comic data via {@link MarvelComicsService#getComicSeries(String, String, String, int)}
     *
     * @param callback callback
     */
    private void loadComicSeriesData(@NonNull Callback<Root> callback, int offset) {
        final MarvelComicsService service = MarvelComicsService.create();
        final String timestamp = String.valueOf(System.currentTimeMillis());
        service.getComicSeries(timestamp, getPublicApiKey(), getHash(timestamp), offset).enqueue(callback);
    }

    @Override
    public void onResponse(@NonNull Call<Root> call, @NonNull Response<Root> response) {
        Log.i(RETROFIT, "Data loaded successfully. " + response.raw());
        if (response.body() == null) {
            return;
        }

        final List<Comic> comics = response.body().getData().getComics();
        mLastOffset = mLastOffset + RANGE;
        setData(comics);
        mLoaded = true;
    }

    private void setData(@NonNull List<Comic> comics) {
        final List<Comic> data = mData.getValue();
        if (data != null) {
            data.addAll(comics);
            mData.setValue(data);
        } else {
            mData.setValue(comics);
        }
    }

    @Override
    public void onFailure(@NonNull Call<Root> call, @NonNull Throwable t) {
        Log.e(RETROFIT, "Failed to load data.", t);
    }
}
