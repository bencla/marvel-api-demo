package sk.danibenc.marvelapidemo.dagger;

import javax.inject.Singleton;

import dagger.Component;
import sk.danibenc.marvelapidemo.MarvelApplication;
import sk.danibenc.marvelapidemo.activity.base.BaseActivity;
import sk.danibenc.marvelapidemo.dagger.module.ApplicationModule;
import sk.danibenc.marvelapidemo.dagger.module.PreferenceModule;
import sk.danibenc.marvelapidemo.dagger.module.RepositoryModule;
import sk.danibenc.marvelapidemo.dagger.module.RetrofitModule;
import sk.danibenc.marvelapidemo.dagger.module.ViewModelModule;
import sk.danibenc.marvelapidemo.fragment.DashboardFragment;
import sk.danibenc.marvelapidemo.fragment.FavouritesFragment;
import sk.danibenc.marvelapidemo.fragment.PreferenceFragment;
import sk.danibenc.marvelapidemo.view.ComicCard;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        PreferenceModule.class,
        RetrofitModule.class,
        RepositoryModule.class,
        ViewModelModule.class
})
public interface ApplicationComponent {

    // Base
    void injectApplication(MarvelApplication application);

    // Activities
    void injectBaseActivity(BaseActivity baseActivity);

    // Fragments
    void injectDashboardFragment(DashboardFragment dashboardFragment);

    void injectFavouritesFragment(FavouritesFragment favouritesFragment);

    void injectPreferenceFragment(PreferenceFragment preferenceFragment);

    // Views
    void injectComicCard(ComicCard comicCard);
}
