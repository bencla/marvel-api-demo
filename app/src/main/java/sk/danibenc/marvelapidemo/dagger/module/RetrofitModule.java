package sk.danibenc.marvelapidemo.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sk.danibenc.marvelapidemo.retrofit.ComicDataLoader;

/**
 * Provides retrofit related classes.
 *
 * @author bencurova
 */
@Module
public class RetrofitModule {

    @Provides
    @Singleton
    ComicDataLoader provideComicDataLoader() {
        return new ComicDataLoader();
    }
}
