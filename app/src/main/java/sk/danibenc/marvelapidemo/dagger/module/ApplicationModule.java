package sk.danibenc.marvelapidemo.dagger.module;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import dagger.Module;
import dagger.Provides;

/**
 * Provides application context.
 *
 * @author bencurova
 */
@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(@NonNull Application application) {
        mApplication = application;
    }

    @Provides
    Context provideApplicationContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }
}
