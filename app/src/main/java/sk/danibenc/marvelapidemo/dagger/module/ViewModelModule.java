package sk.danibenc.marvelapidemo.dagger.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import sk.danibenc.marvelapidemo.model.viewmodel.ComicViewModel;

/**
 * Module for View-Model architecture components that needs to be injected by dagger.
 *
 * Every View-Model that needs to be injected should be placed here and consequently only injection of factory is
 * required during creation of View-Model.
 *
 * @author bencurova
 */
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ComicViewModel.class)
    abstract ViewModel bindAlertActivityModel(ComicViewModel alarmAlertViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(DaggerViewModelFactory factory);
}
