package sk.danibenc.marvelapidemo.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sk.danibenc.marvelapidemo.db.DatabaseCreator;
import sk.danibenc.marvelapidemo.model.repository.ComicRoomRepository;
import sk.danibenc.marvelapidemo.model.repository.base.ComicRepository;
import sk.danibenc.marvelapidemo.retrofit.ComicDataLoader;

/**
 * Module providing the repository.
 *
 * @author bencurova
 */
@Module
public class RepositoryModule {

    @Provides
    @Singleton
    ComicRepository provideComicRepository(DatabaseCreator databaseCreator, ComicDataLoader comicDataLoader){
        return new ComicRoomRepository(databaseCreator, comicDataLoader);
    }
}
