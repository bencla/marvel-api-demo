package sk.danibenc.marvelapidemo.dagger.module;

import android.content.Context;

import javax.inject.Singleton;

import androidx.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import sk.danibenc.marvelapidemo.preferences.AppPreferences;
import sk.danibenc.marvelapidemo.preferences.Preferences;
import sk.danibenc.marvelapidemo.preferences.SharedPreferencesWrapper;

/**
 * Module providing {@link Preferences} and {@link AppPreferences}
 *
 * @author bencurova
 */
@Module
public class PreferenceModule {

    @Provides
    @Singleton
    Preferences providePreferences(@NonNull Context context) {
        return new SharedPreferencesWrapper(context);
    }

    @Provides
    @Singleton
    AppPreferences provideAppPreference(@NonNull Preferences preferences) {
        return new AppPreferences(preferences);
    }
}
