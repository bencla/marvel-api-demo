package sk.danibenc.marvelapidemo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import sk.danibenc.marvelapidemo.MarvelApplication;
import sk.danibenc.marvelapidemo.R;
import sk.danibenc.marvelapidemo.adapter.ComicAdapter;
import sk.danibenc.marvelapidemo.fragment.base.BaseFragment;
import sk.danibenc.marvelapidemo.model.entity.Comic;
import sk.danibenc.marvelapidemo.model.viewmodel.ComicViewModel;
import sk.danibenc.marvelapidemo.listener.EndlessRecyclerOnScrollListener;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Fragment used to display list of Marvel comics.
 *
 * @author bencurova
 */
public class DashboardFragment extends BaseFragment {

    @Inject
    ViewModelProvider.Factory mViewModelProvider;

    @BindView(R.id.comic_list)
    RecyclerView vComicList;

    @BindView(R.id.progress_bar)
    ProgressBar vProgressBar;

    @NonNull
    private final ComicAdapter mAdapter = new ComicAdapter();

    private ComicViewModel mComicViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        injectSelf(view.getContext());

        registerUnbinder(ButterKnife.bind(this, view));

        initRecyclerView();

        subscribeToModel();
    }

    private void injectSelf(@NonNull Context context) {
        MarvelApplication.getApplication(context).getApplicationComponent().injectDashboardFragment(this);
    }

    private void initRecyclerView() {
        vComicList.setLayoutManager(new LinearLayoutManager(getContext()));
        vComicList.setAdapter(mAdapter);
        vComicList.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                mComicViewModel.fetchNewComics();
            }
        });
    }

    private void subscribeToModel() {
        mComicViewModel = ViewModelProviders.of(this, mViewModelProvider).get(ComicViewModel.class);
        mComicViewModel.getComicSeries().observe(this, getComicObserver());
    }

    @NonNull
    private Observer<List<Comic>> getComicObserver() {
        return comics -> {
            if (comics != null) {
                mAdapter.setComics(comics);
            }
            vProgressBar.setVisibility(comics == null ? VISIBLE : GONE);
        };
    }
}
