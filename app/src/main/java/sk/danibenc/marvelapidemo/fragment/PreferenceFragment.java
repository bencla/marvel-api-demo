package sk.danibenc.marvelapidemo.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;
import sk.danibenc.marvelapidemo.MarvelApplication;
import sk.danibenc.marvelapidemo.R;
import sk.danibenc.marvelapidemo.activity.MainActivity;
import sk.danibenc.marvelapidemo.preferences.AppPreferences;
import sk.danibenc.marvelapidemo.util.DialogUtils;

import static sk.danibenc.marvelapidemo.preferences.PreferencesConstants.KEY_USE_DARK_MODE;

/**
 * Preference fragment used to show available preferences to user.
 *
 * @author bencurova
 */
public class PreferenceFragment extends PreferenceFragmentCompat {

    @Inject
    AppPreferences mPreferences;

    private SwitchPreference mUseDarkModePref;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        injectSelf(view.getContext());

        initDarkModePreference();
    }

    private void injectSelf(@NonNull Context context) {
        MarvelApplication.getApplication(context).getApplicationComponent().injectPreferenceFragment(this);
    }

    private void initDarkModePreference() {
        mUseDarkModePref = findPreference(KEY_USE_DARK_MODE);
        mUseDarkModePref.setOnPreferenceChangeListener((preference, newValue) -> {
            showConfirmDialog((Boolean) newValue);
            return false;
        });
    }

    private void showConfirmDialog(boolean newValue) {
        final Context context = getContext();
        if (context == null) {
            return;
        }
        DialogUtils.getConfirmDialog(getString(R.string.use_dark_mode_confirm_dialog_message),
                getPositiveButtonDialogListener(newValue, context), context).show();
    }

    @NonNull
    private DialogInterface.OnClickListener getPositiveButtonDialogListener(boolean newValue, @NonNull Context context) {
        return (dialog, which) -> {
            mUseDarkModePref.setChecked(newValue);
            mPreferences.setDarkThemeEnabled(newValue);
            MainActivity.startActivity(context);
        };
    }
}
