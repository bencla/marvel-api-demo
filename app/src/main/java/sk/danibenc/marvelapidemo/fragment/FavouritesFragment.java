package sk.danibenc.marvelapidemo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import sk.danibenc.marvelapidemo.MarvelApplication;
import sk.danibenc.marvelapidemo.R;
import sk.danibenc.marvelapidemo.adapter.ComicAdapter;
import sk.danibenc.marvelapidemo.fragment.base.BaseFragment;
import sk.danibenc.marvelapidemo.model.entity.Comic;
import sk.danibenc.marvelapidemo.model.viewmodel.ComicViewModel;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Fragment used to show user's favourite comic series.
 *
 * @author bencurova
 */
public class FavouritesFragment extends BaseFragment {

    @Inject
    ViewModelProvider.Factory mViewModelProvider;

    @BindView(R.id.favourites_list)
    RecyclerView vComicList;

    @BindView(R.id.empty_group)
    Group vEmptyGroup;

    @NonNull
    private final ComicAdapter mAdapter = new ComicAdapter();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_favourites, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        injectSelf(view.getContext());

        registerUnbinder(ButterKnife.bind(this, view));

        initRecyclerView();

        subscribeToModel();
    }

    private void injectSelf(@NonNull Context context) {
        MarvelApplication.getApplication(context).getApplicationComponent().injectFavouritesFragment(this);
    }

    private void initRecyclerView() {
        vComicList.setLayoutManager(new LinearLayoutManager(getContext()));
        vComicList.setAdapter(mAdapter);
    }

    private void subscribeToModel() {
        final ComicViewModel mComicViewModel = ViewModelProviders.of(this, mViewModelProvider).get(ComicViewModel.class);
        mComicViewModel.getFavouriteComicSeries().observe(this, getComicObserver());
    }

    @NonNull
    private Observer<List<Comic>> getComicObserver() {
        return comics -> {
            if (comics != null) {
                mAdapter.setComics(comics);
            }
            vEmptyGroup.setVisibility(comics != null && !comics.isEmpty() ? GONE : VISIBLE);
        };
    }
}
