package sk.danibenc.marvelapidemo.fragment.base;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.Unbinder;

/**
 * Base fragment.
 *
 * @author bencurova
 */
public abstract class BaseFragment extends Fragment {

    @Nullable
    private Unbinder mUnbinder;

    protected void registerUnbinder(@NonNull Unbinder unbinder) {
        mUnbinder = unbinder;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}
