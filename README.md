# Marvel API Demo

Demo app using the official [Marvel API](https://developer.marvel.com/)

To use the app, register on Marvel website to generate your own API key.
Then fill it in gradle.properties project file.